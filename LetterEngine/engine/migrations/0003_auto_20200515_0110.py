# Generated by Django 3.0.4 on 2020-05-15 01:10

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0002_auto_20200515_0050'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='artist',
            options={},
        ),
        migrations.RenameField(
            model_name='features',
            old_name='realityTV',
            new_name='realitytv',
        ),
        migrations.RenameField(
            model_name='features',
            old_name='sciFi',
            new_name='scifi',
        ),
    ]
