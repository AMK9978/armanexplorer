from django.urls import path
from rest_framework_simplejwt import views as jwt_views

from .views import *

urlpatterns = [
    path('like', like, name='like'),
    path('seen', seen, name='seen'),
    path('content', content, name='content'),
    path('favorites', get_favorites, name='favorites'),
    path('download', download, name='download'),
    path('login', jwt_views.TokenObtainPairView.as_view(), name='token_create'),
    path('token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path('signup', UserCreate.as_view(), name="create_user"),
    path(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
         activate, name='activate'),
    path('read', read_csv, name='read'),
    path('search', Search.as_view(), name='search'),
    path('profile/<int:user_id>/', ProfileView.as_view()),
    path('board/', BoardListView.as_view()),
    path('board/<int:pk>/', BoardListView.as_view()),
]
