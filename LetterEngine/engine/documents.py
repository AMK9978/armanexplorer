import csv

from django_elasticsearch_dsl import Document, Index
from elasticsearch import Elasticsearch

from .models import *


def indexElasticSearch(dataSetName): #index Elastic Search and dataSetName must be the last dataSet
    es = Elasticsearch([{'host': 'localhost', 'port': 9200}])
    with open(dataSetName, 'r') as DS:
        readear = csv.reader(DS)
        next(readear)
        i= 1
        for line in readear:
            es.index(index='letterengine',doc_type='filmitems',id=i,body={'tconst':line[0],'titleType':line[1],
            'title': line[3], 'Year':line[5], 'genres': line[8],
            'rating':line[9],'Votes':line[10], 'imageURL':line[11],  'summary': line[13]})
            i = i+1

#define a filmitems in index letterengine
filmitems = Index('letterengine')
filmitems.settings(
    number_of_shards=1,
    number_of_replicas=0
)


@ filmitems.doc_type
class ItemDocument(Document):
    class Django:
        model = Item
        fields = [
            "tconst",
            "titleType",
            "genres",
            "title",
            "imageURL",
            "summary",
            "rating",
            "year",
            "votes",
        ]
